$(document).ready(function() {
    customReady();

});

function bedTypeSelect(){
    var bedArray = [];
    var hotelIdArray = [];
    var search_session = JSON.parse( $('#search-session').val() );
    var session = search_session.itinerary;
    var bedTypeUpdate = $("#bedTypeUpdate").val();

    if(bedTypeUpdate == 0){
        if($( ".bedType" ).length > 0){
            $( ".bedType" ).each(function( index ) {
                var room1 = $(this).attr("data-room");
                var hotelId = $(this).attr("data-hotel");
                var leg = $(this).attr("data-leg");
                var hotel_data = search_session.itinerary[leg].hotel;

                var room = {}
                room.roomNo = room1;
                room.bedTypeId = $(this).val();

                if(hotel_data.room){
                    hotel_data.room.push(room);     
                } else {
                    hotel_data.room     = [];
                    hotel_data.room.push(room);
                }
                /*if ( $.inArray(hotelId, hotelIdArray) > -1) {
                    var room = {}
                    room.roomNo = room1;
                    room.bedTypeId = $(this).val();
                    hotel_data.room.push(room);
                    //console.log(hotel_data.room);
                } else {
                    hotelIdArray.push(hotelId);
                    hotel_data.room     = [];
                    var room = {}
                    room.roomNo = room1;
                    room.bedTypeId = $(this).val();
                    hotel_data.room.push(room);  
                } */
            });

            if (session.length > 0) {
                bookingSummary.update(JSON.stringify(search_session));
                $("#bedTypeUpdate").val(1);
                calculateHeight();
                console.log(search_session);
            }
        }
    }
}

function customReady() {
    var totalLeg = $('#last').val();

    for (var i = 0; i <= totalLeg; i++) {
        $("#start_date_" + i).datepicker({
            format: 'd M yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: '+1d'
        }).on('changeDate', function(selected) {
            var selectDate = new Date(selected.date.valueOf());
            var leg = $(this).attr('data-leg');
            var city_id = $(this).attr('data-cityId');
            eroam.ajax('get', 'latest-search', {}, function(response) {
                if (response) {
                    search_session = JSON.parse(response);
                    var session = search_session.itinerary;

                    if (session.length > 0) {
                        if (session[leg].city.id == city_id) {
                            if (leg == 0) {
                                var from_date = selectDate.getFullYear() + '-' + ('0' + (selectDate.getMonth() + 1)).slice(-2) + '-' + ('0' + selectDate.getDate()).slice(-2);
                                search_session.travel_date = from_date;
                            } else {
                                var From_date = new Date($("#start_date_0").attr('data-fromdate'));
                                var To_date = new Date($("#start_date_" + leg).attr('data-fromdate'));
                                var diff_date = To_date - From_date;
                                var legDays = Math.floor(((diff_date % 31536000000) % 2628000000) / 86400000);
                                var from_date = deductDays(selectDate, legDays);
                                search_session.travel_date = from_date;
                            }

                            if (search_session.itinerary[leg].activities != null) {
                                var act = sortByDate(search_session.itinerary[leg].activities);
                                var From_date = new Date($("#start_date_" + leg).attr('data-fromdate'));

                                if (From_date > selectDate) {
                                    var diff_date1 = From_date - selectDate;
                                    var deduct = Math.floor(((diff_date1 % 31536000000) % 2628000000) / 86400000);
                                    var value = $("#start_date_" + leg).attr('data-nights')
                                    var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                    activityCancellation(activityDates, leg, act, value);
                                } else {
                                    bookingSummary.update(JSON.stringify(search_session));
                                }
                            } else {
                                bookingSummary.update(JSON.stringify(search_session));
                            }
                        }
                    }
                }
            });
        });
    };

    var currentPath = $('#currentPath').val();
    if (currentPath == 'map') {
        $('.booking-summary').slimScroll({
            height: '1000px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
    }

    var page = $('#itinerary-page').val();

    if (page == '' || page == undefined) {
        $('[data-target="#demo0"]').trigger('click');
        //$('#demo0').addClass('show');
        //$('#demo0').prev('div').addClass('itinerary_left_active');
        ///$('#demo0').prev('div').find('.ic-up_down').addClass('ic-down_up').removeClass('ic-up_down');
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
}


function calculateHeight() {
    setTimeout(function(){ timeOutCalculateHeight(); },2000);
}

function timeOutCalculateHeight() {
    var winHeight = $(window).height();
    var oheight = $('.booking-summary').outerHeight();
    $('.itinerary_right').outerHeight(oheight);
    var elem = $('.itinerary_right').outerHeight();
    var filter_height = $('.itinerary_filter').outerHeight();
    var elemHeight = oheight - filter_height;
    
    $('.itinerary_right').outerHeight(oheight);
    $(".slimScrollDiv").css({'height':'auto'});
    $(".itinerary_page").css({'height':(elemHeight-16),'padding-bottom':'1rem'});
}

$(function() {
    /* Start - BedType Selection & update - Added by Rekha Patel - 24/09/2018 */
        bedTypeSelect();
        $('body').on("change",".bedType", function() {
            //$(".loader").show();
            var room = $(this).attr("data-room");
            var leg = $(this).attr("data-leg");
            var bedTypeId = $(this).val();
            var search_session = JSON.parse( $('#search-session').val() );
            var hotel_data = search_session.itinerary[leg].hotel.room;
            $.each(hotel_data, function( key, value ) {
                if(value.roomNo == room){
                    hotel_data[key].bedTypeId = bedTypeId;
                    bookingSummary.update(JSON.stringify(search_session));
                    calculateHeight();
                }
            }); 
            //setTimeout( function() { $(".loader").hide(); }, 200 );
        });

        $('body').on("blur",".specialInsExpedia", function() {
            var leg = $(this).attr("data-leg");
            var search_session = JSON.parse( $('#search-session').val() );
            var hotel_data = search_session.itinerary[leg].hotel;

            if(hotel_data.specialInformation != $(this).val()){
                hotel_data.specialInformation = $(this).val();
                bookingSummary.update(JSON.stringify(search_session)); 
                calculateHeight();
            }
        });

        $(document).on('change','.location-select', function(){
            var val = $(this).val();
            if(val == 1)
            {
                hotel_pickup = "pickup-"+$(this).data('tourcode');
                $(".choose_location_"+$(this).data('tourcode')).show();
            }
            else
            {
                $(".choose_location_"+$(this).data('tourcode')).hide();
                $("#notListed_"+$(this).data('tourcode')).hide();
                $(".manual-pickup-"+$(this).data('tourcode')).val('');
            }
        });

        /*$('body').on('click','.saveInstruction', function(e){
            var href = $(this).attr("href");
            $(".loader").show();
            var search_session = JSON.parse( $('#search-session').val() );
            var session = search_session.itinerary;
        
            if($( ".specialInsExpedia" ).length > 0){
                $( ".specialInsExpedia" ).each(function( index ) {
                    var leg = $(this).attr("data-leg"); 
                    var hotel_data = search_session.itinerary[leg].hotel;

                    if(hotel_data.specialInformation != $(this).val()){
                        hotel_data.specialInformation = $(this).val();
                    }
                }); 
            } 

            if (session.length > 0) {
                bookingSummary.update(JSON.stringify(search_session)); 
                calculateHeight();
                $(document).ajaxStop(function() {
                    console.log(search_session);
                    $(".loader").hide();
                    window.location.href = href;
                });
            } 
        });*/
    /* End - BedType Selection & update - Added by Rekha Patel - 24/09/2018 */

    
    $('body').on('click','.btn_update_iti', function(e){
        e.preventDefault();
        var leg = $(this).attr('data-leg');
        var tourCode = $(this).attr('data-code');
        var search_session = JSON.parse( $('#search-session').val() );
        var data = $(this).data();
        var activity_data = search_session.itinerary[leg].activities[data.akey];
        
        var form = $( "#form-"+tourCode );
        var validateForm = form.valid(); 
        var weightValidate = $(this).attr('data-weight-validate');
        
        var countAdult = $('.pass-fnm-aAdults-'+tourCode).length;
        var countChild = $('.pass-fnm-aChilds-'+tourCode).length;
        var countInfant = $('.pass-fnm-aInfants-'+tourCode).length;
        var paxInfo = [];
        var activityBookingQuestion = [];
        var finalArray = [];  

        if(validateForm == true)
        {
            if(countAdult > 0)
            {
                var passenger = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                        passenger[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aAdults-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aAdults-'+tourCode).each(function () { 
                        passenger[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aAdults-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aAdults-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                }                                   
            }

            if(countChild > 0)
            {
                var passengerc = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                        passengerc[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aChilds-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aChilds-'+tourCode).each(function () { 
                        passengerc[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aChilds-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aChilds-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                } 
            }

            if(countInfant > 0)
            {
                var passengeri = [];
                var i = 0;
                if(weightValidate == 1)
                {
                    $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                        passengeri[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val(),
                           'bookingQuestions' : {
                                'questionID' : 23,
                                'answer' : $('.weight-aInfants-'+tourCode+'-'+i).val()+' '+ $(".weight_select_"+tourCode).val()
                           }
                        };
                        i++;                    
                    });
                }
                else
                {
                    $('.pass-fnm-aInfants-'+tourCode).each(function () { 
                        passengeri[i] = {
                           'firstname'  : $(this).val(),
                           'lastname'   : $('.pass-lnm-aInfants-'+tourCode+'-'+i).val(),
                           'age'        : $('.pass-age-aInfants-'+tourCode+'-'+i).val()
                        };
                        i++;                    
                    });
                }                                   
            }                

            
            paxInfo = {
                'aAdults' : passenger,
                'aChilds' : passengerc,
                'aInfants' : passengeri
            }    
            

            var hotelPickup = [];
            if(data.pickup == "1")
            {
                var location_select_val = $("input:radio.location-select-"+tourCode+":checked").val();
                if(location_select_val == 1)
                {
                    if($(".pickup-"+tourCode).val() == 'notListed')
                    {
                        hotelPickup = {
                            'hotelId' : $(".pickup-"+tourCode).val(),
                            'pickupPoint' :$(".manual-pickup-"+tourCode).val()
                        }
                    }
                    else
                    {
                        hotelPickup = {
                            'hotelId' : $(".pickup-"+tourCode).val()
                        }
                    }
                }

                if ( $(".compli-pickup-"+tourCode).length ) 
                {
                    hotelPickup = {
                            'pickupPoint' : $(".compli-pickup-"+tourCode).val()
                        }
                }
            }
            var question = [];
            var l = 0;
            $('.booking_'+tourCode).each(function () {                     
                question[l] = {
                   'questionId'  : $(this).data('question'),
                   'answer'   : $(this).val(),
                };
                l++;
            });
            activityBookingQuestion.push(question);
            
            var specialRequirement = $(".special_req_"+tourCode).val();

            finalArray = {
                'paxInfo' : paxInfo,
                'hotelPickup' : hotelPickup,
                'bookingQuestions' : question,
                'specialRequirement' : specialRequirement
            };

            activity_data.specialRequirement = finalArray.specialRequirement;
            activity_data.paxInfo = finalArray.paxInfo;
            activity_data.bookingQuestionAnswers = finalArray.bookingQuestions;
            activity_data.hotelPickups = finalArray.hotelPickup;
            $(".loader").show();
            bookingSummary.update(JSON.stringify(search_session));  
            
            $(document).ajaxStop(function() {
                $(".loader").hide();
                var session_search = JSON.stringify( search_session );                
                $('#search-session').val(session_search);
                //parent.location.reload();
            });
        }
        
    });

    $(document).on('change','.pickup-hotel', function(){
        var val = $(this).val();   
        if(val == 'notListed')
        {
            $("#notListed_"+$(this).data('tourcode')).show();
        }
        else
        {
            $("#notListed_"+$(this).data('tourcode')).hide();
        }
    });    

    $("#departure_date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $("#return_date").datepicker({
        format: 'dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });

    var currentPath = $('#currentPath').val();

    if (currentPath == 'map') {
        $('.booking-summary').slimScroll({
            height: '1000px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
    }

    if (currentPath == 'payment-summary') {
        $(".itinerary_page").slimScroll({
            height: '1200px',
            color: '#212121',
            opacity: '0.7',
            size: '5px',
            allowPageScroll: true
        });
    }
    

    function reload() {
        window.location.reload();
    }

    $('body').on("change",".changeNights", function() {

        var leg = $(this).attr('data-leg');
        var value = $(this).val();
        var city_id = $(this).attr('data-cityId');
        var from_date = $(this).attr('data-fromDate');
        var to_date = $(this).attr('data-toDate');
        eroam.ajax('get', 'latest-search', {}, function(response) {
            if (response) {
                search_session = JSON.parse(response);
                var session = search_session.itinerary;
                if (session.length > 0) {
                    if (session[leg].city.id == city_id) {
                        var nights = search_session.itinerary[leg].city.default_nights;
                        if ((search_session.itinerary[leg].hotel != null) && typeof search_session.itinerary[leg].hotel.nights != 'undefined') {
                            nights = search_session.itinerary[leg].hotel.nights;
                            search_session.itinerary[leg].hotel.nights = value;
                            var days_to_add = parseInt(value) - parseInt(nights);
                            if (days_to_add > 0) {
                                search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) + days_to_add;
                                search_session.itinerary[leg].city.days_to_add = days_to_add;
                                search_session.itinerary[leg].city.add_after_date = from_date;                                
                                bookingSummary.update(JSON.stringify(search_session));
                                setTimeout(function() {
                                    parent.location.reload();
                                }, 2000);
                            } else {
                                var deduct = Math.abs(days_to_add);
                                var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);
                                search_session.itinerary[leg].city.days_to_deduct = deduct;
                                search_session.itinerary[leg].city.deduct_after_date = from_date;
                                search_session.itinerary[leg].city.default_nights = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);
                                if (search_session.itinerary[leg].activities != null) {
                                    var act = sortByDate(search_session.itinerary[leg].activities);
                                    var departureDate = getDeparture(leg, from_date, to_date);
                                    departureDate = departureDate.split(' ');
                                    var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                    //alert(departureDate + '==//==' + activityDates);
                                    activityCancellation(activityDates, leg, act, value);
                                } else {
                                    bookingSummary.update(JSON.stringify(search_session),false,reload);
                                }
                                
                                //location.reload();

                                return;
                            }
                        } else {
                            /*console.log(search_session);return false;
                            search_session = JSON.parse(response); 
                            var value1 = parseInt(value);
                            search_session.itinerary[leg].city.default_nights = value1;
                            $('.number-of-nights[data-index="' + leg + '"]').attr('data-nights', value1);
                            $('#night-data-'+leg).val(value1);
                            search_session.itinerary[leg].city.add_after_date = search_session.itinerary[leg].city.date_from;
                            search_session.itinerary[leg].city.days_to_add = value1;
                            var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                            var toDate = incrementDate(search_session.itinerary[leg].city.date_to,1);
                            var todate = toDate.toDateString().split(' ');
                            var toDateNew = todate[1] + ' ' + todate[2] + ' ' + todate[3];
                            var fromDate = search_session.itinerary[leg].city.date_from.split('-');
                            var fromDateNew = fromDate[2] + ' ' + monthNames[fromDate[1] - 1] + ' ' + fromDate[0];
                            var nightsText = search_session.itinerary[leg].city.default_nights == 1 ? + ' Night' : ' Nights';
                            var dateUpdate = fromDateNew+ ' - '+toDateNew+' ('+search_session.itinerary[leg].city.default_nights + nightsText+')';
                            $('.dateUpdate').text(dateUpdate);*/

                            //New section from above
                            search_session = JSON.parse(response); 
                            var value1 = parseInt(value);
                            nights = search_session.itinerary[leg].city.default_nights;
                            search_session.itinerary[leg].city.default_nights = value1;
                            $('.number-of-nights[data-index="' + leg + '"]').attr('data-nights', value1);
                            $('#night-data-'+leg).val(value1);

                            var days_to_add = parseInt(value1) - parseInt(nights);
                            if (days_to_add > 0) {
                                search_session.itinerary[leg].city.default_nights = value1;
                                search_session.itinerary[leg].city.days_to_add = days_to_add;
                                search_session.itinerary[leg].city.add_after_date = from_date;
                                bookingSummary.update(JSON.stringify(search_session),false,reload);
                            } else {  
                                var deduct = Math.abs(days_to_add);
                                var val = parseInt(search_session.itinerary[leg].city.default_nights) - parseInt(deduct);
                                search_session.itinerary[leg].city.days_to_deduct = deduct;
                                search_session.itinerary[leg].city.deduct_after_date = from_date;
                                search_session.itinerary[leg].city.default_nights = value1;
                                if (search_session.itinerary[leg].activities != null) { 
                                    var act = sortByDate(search_session.itinerary[leg].activities);
                                    var departureDate = getDeparture(leg, from_date, to_date);
                                    departureDate = departureDate.split(' ');
                                    var activityDates = getActivityToBeCancelledByDates(search_session.itinerary[leg].city.date_to, parseInt(deduct));
                                    activityCancellation(activityDates, leg, act, value);
                                } else { 
                                    bookingSummary.update(JSON.stringify(search_session),false,reload);
                                }
                                return;
                            }
                            //finish
                            //bookingSummary.update(JSON.stringify(search_session),false,reload);

                        }
                    }

                }
            }
        });
    });  
})

function getDeparture(itineraryIndex, from_date, to_date) {
    var search_session = JSON.parse($('#search-session').val());
    var newDateTo = addDays(from_date, parseInt(search_session.itinerary[itineraryIndex].city.default_nights));
    var transport = search_session.itinerary[itineraryIndex].transport;

    var departureDate = to_date + ' 24';
    if (transport != null) {
        var eta = transport.eta;
        var travelDuration = transport.duration;
        if (transport.provider == 'mystifly') {
            travelDuration = formatTime(get_hours_min(travelDuration));

            eta = moment(eta, moment.ISO_8601);
            eta = eta.format('hh:mm A');

        } else {
            travelDuration = removePlus(travelDuration);
            travelDuration = formatTime(travelDuration);
        }

        var arrivalTime = arrival_am_pm(eta);
        departureDate = getDayTime(newDateTo + ' ' + arrivalTime, travelDuration);
    }
    return departureDate;
}
function get_hours_min(hour_min){
    var hours_mins = hour_min.toString().split(".");
    var hours = parseInt(hours_mins[0]);
    var mins = parseFloat('0.'+hours_mins[1]) * 60;

    return hours+' hr(s) '+Math.ceil(mins)+' min(s)';
}

function removePlus(time){
    return time.replace( /\+/g, '' );
}
function formatTime(time){
    return time.replace(/hr\(s\)|min\(s\)|hours|minutes /gi, function(x){
        return x == 'hr(s)' || 'hours' ? ':' : '';
    });
}
function arrival_am_pm(eta){

    var result;
    var arrival_split = eta.split("+");
    var arrival_split_am_pm = arrival_split[0].split(" ");
    var arrival_split_hour_min = arrival_split[0].split(":");
    var arrival_hour = parseInt(arrival_split_hour_min[0]);
    var arrival_min = arrival_split_hour_min[1].replace(/pm|am| /gi, '');


    if(typeof arrival_split_am_pm[1] !== 'undefined'){

        result = padd_zero(arrival_hour)+':'+arrival_min+' '+arrival_split_am_pm[1];
    }else{
        if( arrival_hour < 12 ){
            result = padd_zero(arrival_hour)+':'+arrival_min+' AM';
        }else{
            hour = arrival_hour - 12;
            result = padd_zero(hour)+':'+arrival_min+' PM';
        }
    }

    return result;
}
function padd_zero(number) {
    if(parseInt(number) == 0){
        number = 12;
    }
    return (number < 10) ? ("0" + number) : number;
}
function sortByDate(activities){
    var sorted = activities.sort(function (itemA, itemB) {
        var A = itemA.date_selected;
        var B = itemB.date_selected;
        return A.localeCompare(B);
    });
   return sorted;
}
function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + parseInt(days) );
    return dateFormatter(result);
}
function getDayTime(arrivalDate, duration) {
    var hours_mins = duration.split(':');
    var result = new Date(arrivalDate);
    result.setHours(result.getHours() - parseInt(hours_mins[0]));
    result.setMinutes(result.getMinutes() - parseInt(hours_mins[1]));

    return dateFormatter(result, true);
}
function dateFormatter(date, time = false) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if(time){
        day += ' '+ d.getHours();
    }
    return [year, month, day].join( '-' );
}
function getActivityToBeCancelledByDates( dateTo, numberOfDays ){
    var dateArray = [];
    if( numberOfDays ){
        dateArray.push(dateTo);
        for( var count = 1;count <= numberOfDays; count ++ ){
            dateArray.push( deductDays( dateTo, count) )
        }
    }
    return dateArray;
}
function deductDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() - parseInt(days) );
    return formatDate(result);
}
function formatDate(date, time = false) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    if (time) {
        day += ' ' + d.getHours();
    }
    return [year, month, day].join('-');
}
function reload() {
    window.location.reload();
}
 
function activityCancellation(activityDates, itineraryIndex, act, value) { 
    if (activityDates.length > 0) {
        eroam.confirm(null, 'Decreasing the number of nights can cause schedule conflicts for the activities on this city. Would you like to cancel some activities for this city?', function(e) {
            if (act.length > 0) {
                for (index = act.length - 1; index >= 0; index--) {
                    var actDuration = parseInt(act[index].duration);
                    var multiDayRemoved = false;
                    if (actDuration > 1) {
                        actDuration = actDuration - 1;
                        var activityDateSelected = act[index].date_selected;
                        for (var counter = 1; counter <= actDuration; counter++) {
                            var multiDayActivityDate = addDays(activityDateSelected, counter);
                            if (activityDates.indexOf(multiDayActivityDate) > -1 && !multiDayRemoved) {
                                act.splice(index, 1);
                                multiDayRemoved = true;
                            }
                        }
                    } else {
                        if (activityDates.indexOf(act[index].date_selected) > -1) {
                            act.splice(index, 1);
                        }
                    }
                }
            }

            search_session.itinerary[itineraryIndex].activities = act;

            bookingSummary.update(JSON.stringify(search_session));
            setTimeout(function() {
                parent.location.reload();
            }, 2000);
        });
    } else {
        bookingSummary.update(JSON.stringify(search_session),false,reload);
    }
}


$('body').on('click', '.saveInstruction', function(e) {
    var href = $(this).data("location");
    console.log(href);
    var form_validate = true;
    $('form').each(function() {
        var form = $(this).valid();
        if(form == false)
        {
            form_validate = form;
        }
    });
    if(form_validate == true)
    {        
        window.location.href = href;
    }
});

$('body').on('click', '#reorder-locations-btn', function(e) {
    e.preventDefault();
    $('.itinerary-container, #reorder-locations-btn').hide();
    $('#reorder-locations-container').show();
    $('#reorder-locations').sortable({
        items: 'li:not(li:first-child)',
        placeholder: 'ui-state-highlight',
        opacity: 0.7,
        scrollSensitivity: 50,
        forcePlaceholderSize: true,
		start: function( event, ui ) { 
			$(ui.item).addClass("color_class");
		},
		stop:function( event, ui ) { 
			$(ui.item).removeClass("color_class");
		},
        over: function(e, ui) {
            ui.placeholder.height(ui.item.height() - 30);
        }
    });
});

$('body').on('click', '#cancel-reorder-btn', function(e) {
    e.preventDefault();
    $('#reorder-locations-container').hide();
    $('.itinerary-container, #reorder-locations-btn').show();
});

$('body').on('click', '.accommodation_outer', function(e){
    var url = $(this).children('.accommodation').data("link");
    if (url != undefined) {
        window.location.href = url;
    }
});

$('body').on('click', '[data-target="#details"]', function() {
	if ($(this).hasClass('collapsed')) { 
		$(this).find('i').removeClass('ic-up_down').addClass('ic-down_up');
	}else{
	    $(this).find('i').removeClass('ic-down_up').addClass('ic-up_down');
	}
});

// Collapse click
$('body').on('click', '.itinerary-container .collapse_click', function() {
    if ($(this).hasClass('collapsed')) {
		$('.itinerary-container .collapse_click').find('i').removeClass('ic-down_up').addClass('ic-up_down');
        $(this).find('i').addClass('ic-down_up').removeClass('ic-up_down');
    }else{ 
		$('.itinerary-container .collapse_click').find('i').removeClass('ic-down_up').addClass('ic-up_down');
        $(this).find('i').addClass('ic-up_down').removeClass('ic-down_up');
       
    }
    $(".itinerary_left_box").each(function() {
        $(this).removeClass('itinerary_left_active');
    });
    $(this).parent('.itinerary_left_box').addClass('itinerary_left_active');
});

$("#login_form2").validate({

    rules: {
        username_login: {
            required: true,
            email:true
        },
        password_login: {
            required: true,
        }
    },
    errorPlacement: function (label, element) {
        label.insertAfter(element);
    },
    submitHandler: function (form) {
        
        $('input.submitLoginForm').val('Loading');
        var postData = $("#login_form2").serialize();
            eroam.ajax('post', 'login', postData, function(response) {
                $('#login-btn2').html('Loading...');
                if (response.trim() == 'valid') {
                    $('#login-error-msg2').hide();
                    $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Logging in...');
                    window.location.href = $("#current_page").val();
                }
                else if(response.trim() == 'confirm_first')
                {
                    $('#login-error-message2').text('Please Confirm your mail first.').show();
                    $('#login-btn2').html('Log In');
                }
                else {
                    $('#login-error-message2').text('Invalid Email Address or Password').show();
                    $('#login-btn2').html('Log In');
                }
            }, function() {
                $('#login-btn2').html('<i class="fa fa-circle-o-notch fa-spin"></i> Checking User...');
            });
        }
});

jQuery.validator.addMethod("lettersonly", function(value, element) {
          return this.optional(element) || /^[a-z]+$/i.test(value);
        }, "Please enter letters only."); 

jQuery.validator.addMethod("numbersonly",function(value,element) {
    return this.optional(element) || /^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$/.test(value);
}, "Please enter valid number");

jQuery.validator.addMethod("creditcardnumber", function(value, element) {
    return this.optional(element) || ($('#card_valid').val() == 'true');
}, "Invalid credit card number");


if($("#card_number").length != 0) {
    $('#card_number').validateCreditCard(function(result){
        $('#card_valid').val(result.valid);
    });
}

var getDateEnd = $('#getDateEnd').val();
jQuery.validator.addMethod("passportnumber",function(value,element) {
    return this.optional(element) || /^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$/.test(value);
}, "Invalid passport number");

$.validator.addMethod("regex", function(value, element, regexpr) {      
     return regexpr.test(value);
}, "Please enter a valid pasword.");    
    
jQuery.validator.addMethod("greaterThan", 
function(value, element, params) { 
    var date1 = params.split("-");
    var date2 = value.split("-");
    var max = new Date(date1[2],date1[1],date1[0]);
    var min = new Date(date2[2],date2[1],date2[0]);
    return min > max;
},'Must be greater than {0}.');

jQuery.validator.addMethod("checkInfant", 
function(value, element, params) { 
    var limit = parseInt(params.limit);
    var date1 = params.lastTravel.split("-");
    var date2 = value.split("-");
    var max = new Date(date1[2],date1[1],date1[0]);
    var min = new Date(date2[2],date2[1],date2[0]);
    var today = new Date();
    if (limit <= 2) {
        if (min >= max) {
            return false;
        }else{
            var timeDiff = Math.abs(max.getTime() - min.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if(diffDays < 730){
                return true;
            }else{
                return false;
            }
        }
    }else if(limit > 2 && limit <= 12){ 
        if (min >= max) {
            return false;
        }else{
            var timeDiff = Math.abs(max.getTime() - min.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            if(diffDays < (365*12)){
                return true;
            }else{
                return false;
            }
        }
    }else{
        return true;
    }
},'Invalid date.');

$(document).ready(function() {

    $('.expired_on').change(function(){
        $(this).valid();
    });

    $('.passenger_dob').change(function(){
        $(this).valid();
    });

    $("#signup_form_id").validate({
        ignore: [],
        rules: {
            passenger_address_one: {
                required: true
            },
            passenger_suburb: {
                required: true
            },
            passenger_state: {
                required: true
            },
            passenger_zip: {
                required: true,
                number:true
            },
            'passenger_title[]': {
                required: true
            },
            'passenger_first_name[]': {
                required: true
            },
            'passenger_last_name[]': {
                required: true
            },
            'dob[]': {
                required: true
            },
            'age[]': {
                required: true
            },
            'email[]': {
                required: true
            },
            'phone[]': {
                required: true
            },
            'country[]': {
                required: true
            },
            'area[]': {
                required: true
            },
            'gender[]': {
                required: true
            },
            'meal[]': {
                required: true
            },
            'passport_date[]': {
                required: true
            },
            'passport_no[]': {
                required: true
            },
            'child_title[]': {
                required: true
            },
            'child_first_name[]': {
                required: true
            },
            'child_last_name[]': {
                required: true
            },
            'child_dob[]': {
                required: true
            },
            'child_age[]': {
                required: true
            },
            'child_email[]': {
                required: true
            },
            'child_phone[]': {
                required: true
            },
            'child_meal[]': {
                required: true
            },
            'child_passport_no[]': {
                required: true
            },
            'child_phone[]': {
                required: true
            },
            first_name: {
                required: true,
                lettersonly:true
            },
            last_name: {
                required: true,
                lettersonly:true
            },
            card_number: {
                required: true,
                number:true,
                maxlength: 16,
                creditcardnumber: true
            },
            year: {
                required: true
            },
            cvv: {
                required: true,
                maxlength: 3,
                number:true
            },
            month: {
                required: true
            },
            country: {
                required: true
            },
            postalcode: {
                required: true
            },
            passenger_country: {
                required: true
            },
            terms: {
                required: true
            }
        },
        errorPlacement: function (label, element) {
            if(element.hasClass('terms')) {
                 label.insertAfter(element.closest('span').next('a'));
            } else {
                label.insertAfter($(element).parents('div.fildes_outer'));
                
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
        invalidHandler: function(e,validator) {
            for (var i=0;i<validator.errorList.length;i++){   
                $(validator.errorList[i].element).parents('.collapse').collapse('show');
            }
        }
    });

    $('.passenger_zip').each(function () {
        $(this).rules('add', {
            required: true,
            number:true
        });
    });
    $('.passenger_state').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_suburb').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_address_one').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    /*
    $('.passenger_gender').each(function () {
        $(this).rules('add', {
            required: true
        });
    });*/
    $('.passenger_title').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_first_name').each(function () {
        $(this).rules('add', {
            required: true,
            lettersonly:true
        });
    });
    $('.passenger_last_name').each(function () {
        $(this).rules('add', {
            required: true,
            lettersonly:true
        });
    });
    $('.passenger_dob').each(function () {
        $(this).rules('add', {
            required: true,
        });
    });
    /*$('.passport_no').each(function () {
        $(this).rules('add', {
            required: true,
            passportnumber:true
        });
    });
    $('.expired_on').each(function () { 
        $(this).rules('add', {
            required: true,
            greaterThan: getDateEnd
        });
    });
    $('.issue_country').each(function () {
        $(this).rules('add', {
            required: true
        });
    }); 
    $('.meal_preference').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.post_code').each(function () {
        $(this).rules('add', {
            required: true,
            digits: true
        });
    });
    $('.notes').each(function () {
        $(this).rules('add', {
            required: true
        });
    });*/
    $('.passenger_contact_no').each(function () {
        $(this).rules('add', {
            required: true,
            numbersonly:true,
            maxlength: 15,
        });
    });
    $('.passenger_email').each(function () {
        $(this).rules('add', {
            required: true,
            email:true
        });
    });
    $('.passenger_country').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    /*$('.passenger_nationality').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_passport_expiry_date').each(function () {
        $(this).rules('add', {
            required: true
        });
    });
    $('.passenger_passport_num').each(function () {
        $(this).rules('add', {
            required: true
        });
    });*/
});

// $(document).on('click','.itinerary_left_box',function(evt) {
//     evt.preventDefault();
//     $('.itinerary_left_box').each(function() {
// 		$(this).removeClass('itinerary_left_active');
// 	});
//     $(this).addClass('itinerary_left_active');
// });

$('.check_indicator').click(function(){ 
    var element = $(this).closest('.custom_check').find('input[type="checkbox"]');
    if(element.prop('checked')==true){ 
         element.prop('checked', false);
    }else{
       element.prop('checked', true);
    }
});