<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $data['city1'].'_HotelVoucher_'.$data['itineraryId'].'.pdf';?></title>
    <style type="text/css">
      @page {
        margin: 15px;
      }
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <?php 
            $logo = getDomainLogo();
            if(!$logo) {
                $logo = public_path('/images/logo-big.png');
            }
    ?>
    <table width="100%" cellpadding="4">
      <tbody>
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 0px 15px; vertical-align: middle;">
                  <h2 style="margin-top: 4px;"><img src="{{ public_path('/images/ic_local_hotel.svg') }}" alt="" style="position: relative; top: 3px; margin-top: 4px;"/> Hotel Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa;text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Booking Details</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p><strong>Guest Name:</strong> {{$data['name']}}</p>
                    <p><strong>Email:</strong> {{$data['email']}}</p>
                    <p><strong>Affiliate Confirmation Id :</strong> {{$data['affiliateConfirmationId']}}</p>
                  </td>
                  <td>
                      <p><strong>eRoam Booking Id:</strong> {{$data['invoiceNumber']}}</p>
                      <p><strong>Booking Date:</strong> {{$data['bookingDate']}}</p>
                      <p><strong>Hotel Booking ID:</strong>{{$data['bookingId']}}</p>
                  </td>
                </tr>
                <tr>
                  <td>
                      <h3 style="margin: 0;">{{$data['city1'].', '.$data['country_code'].' - '.$data['hotelName']}}</h3>
                  </td>
                  <td></td>
                </tr>
                <tr>
                    <td colspan="3">
                      <strong>Hotel Address</strong><br>
                      {{$data['address']}}<br>{{$data['city'].', '.$data['city1'].', '.$data['country_code']}}
                    </td>
                </tr>
                <tr>
                  <td colspan="3">
                      @php 
                        $price = $data['price'];
                        $total = $price + $data['taxes'];
                        $hotelFee = $total + $data['HotelFees'];
                        $totaltax =  $data['taxes']+$data['HotelFees'];

                        $price = number_format($price,2);
                        $total = number_format($total,2);
                        $hotelFee = number_format($hotelFee,2);
                        $totaltax = number_format($totaltax,2);
                      @endphp
                    <h3 style="margin: 5px 0">Total payment to be made - {{$data['currencyCode']}} {{$total}}</h3>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" border="1" style="border: solid 3px #fafafa; text-align: center; border-collapse: collapse; margin-top: 5px;" cellspacing="4" cellpadding="10">
              <tr>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Check-in Date<!--  &amp; Time --></h3>
                    <p>{{$data['checkIn']}}</p>
                    <!-- <p>12 PM Onwards</p> -->
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Nights</h3>
                    <p>{{$data['nights']}} Nights</p>
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Check-out Date<!--  &amp; Time --></h3>
                    <p>{{$data['checkOut']}}</p> 
                    <!-- <p>Till 11 AM</p> -->
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Guests</h3>
                    <p>{{$data['numberOfAdults']}} Adult {{!empty($data['numberOfChildren']) ? ', '.$data['numberOfChildren'].' Child':''}}</p>
                </td>
                <td style="border-color:#fafafa;">
                    <h3 style="margin: 0;">Rooms</h3>
                    <p>{{$data['rooms']}} {{($data['rooms'] > 1) ? 'Rooms':'Room'}}</p>
                    @isset($data['bedTypeDescription'])
                    <p><strong>{{$data['bedTypeDescription']}}</strong></p>
                    @endisset
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="3">
                      <h2 style="margin: 8px 8px 8px 2px;">Room Details</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $passenger = session()->get('search_input')['pax'];

                    for($i = 0; $i < $data['rooms']; $i++){
                      $roomtype = '<tr><td><h3 style="margin: 5px 0">Room '.($i+1).' </h3>';
                      $roomtype .= '<table>';
                      $roomtype .= '<tr><td width="50px">&nbsp;</td><th>Room Type</th><th>:</th><td>'.$data['roomDescription'].'</td></tr>';
                      $roomtype .= '<tr><td></td><th>'. __('home.Bed_Preferences').'</th><th>:</th><td>';

                      if(isset($data['BedTypes'])){
                        if($data['BedTypes']['@size'] > 1){
                          foreach($data['BedTypes']['BedType'] as $bedType){
                            if(isset($data['room']) && $bedType['@id'] == $data['room'][$i]['bedTypeId']){
                              $roomtype .= ucwords($bedType['description']); break;
                            } /*else {
                              $roomtype .= ucwords($bedType['description']); break;
                            }*/
                          } 
                        }else {
                          $roomtype .= ucwords($data['BedTypes']['BedType']['description']);
                        }
                      }

                      $roomtype .='</td></tr>';
                      $roomtype .= '<tr><td></td><th>Passengers Detail</th><th>:</th><td>';
                      if(isset($passenger[$i])){
                        $j=0;
                        foreach ($passenger[$i] as $pass) {
                          $j++;
                          $roomtype .= ucwords($pass['firstname'].' '.$pass['lastname']);//.' ('.$pass['age'].')';
                          if($pass['child'] == 1){
                            if($pass['age'] == 0){ $pass['age'] = 1; }
                            $roomtype .= ' ('.$pass['age'].($pass['age'] > 1 ? ' Years':' Year').')';
                            
                          }
                          if($j < count($passenger[$i])) { $roomtype .= ', ';} else { $roomtype .= '</p>';}
                        }
                      }

                      $roomtype .='</td></tr>';
                      $roomtype .= '<tr><td></td><th>Confirmation Number</th><th>:</th><td>'.$data['confirmationNumber'][$i].'</td></tr>';
                      $roomtype .='</table>';
                      $roomtype .= '</td></tr>';
                      echo $roomtype;
                    }
                  ?>  
                </tbody>
              </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="3">
                      <h2 style="margin: 8px 8px 8px 2px;">Payment Summary</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <strong>Room Tariff</strong>
                    </td>
                    <td>
                      <?php 
            						$perPersion = @((($data['price']) / ($data['rooms']))/$data['nights']); 
            						$perPersion = number_format($perPersion,2);
                        
                        echo $data['currencyCode'].' '.$perPersion.' x '.$data['nights'].' Nights x '. $data['rooms'].' Rooms';

                      ?>
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                        {{$data['currencyCode'].' '.$price}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Other Charges</strong><br>
                      Tax Recovery & Service Fee<br>
                      Hotel Fees
                    </td>
                    <td><br>
                      {{$data['currencyCode'].' '.number_format($data['taxes'],2)}}<br>
                      {{$data['currencyCode'].' '.number_format($data['HotelFees'],2)}}
                    </td>
                    <td style="text-align: right; padding-right: 20px;">	
                      <strong>{{$data['currencyCode'].' '.$totaltax}}</strong><br>
                      {{--$data['currencyCode'].' '.number_format($data['taxes'],2)--}}<br>
                      {{--$data['currencyCode'].' '.number_format($data['HotelFees'],2)--}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Grand Total</strong>
                    </td>
                    <td></td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{$data['currencyCode']}} {{$hotelFee}}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Advance Paid</strong>
                    </td>
                    <td>
                      Paid through Credit Card ({{$data['currencyCode'].' '.$total}})
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      {{$data['currencyCode']}} {{$total}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Total Payable Amount </strong><br>All Inclusive
                    </td>
                    <td>
                      <strong>Pay at Check-in</strong>
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{$data['currencyCode'].' '.number_format($data['HotelFees'],2)}}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>

        <?php if(isset($data['checkInInstructions']) || isset($data['specialCheckInInstructions'])){?> 
          <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th>
                      <h2 style="margin: 8px 8px 8px 2px;">Additional Details or Extra Charges</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @isset($data['checkInInstructions'])
                    <tr>
                      <td>
                        <h3 style="margin: 0;">@lang('home.hotel_checkin_instruction_text')</h3>
                        <p style="margin-left: 15px;">
                          @php
                            $data['checkInInstructions'] = strstr($data['checkInInstructions'], '<p><b>Fees</b>');
                            $data['checkInInstructions'] = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$data['checkInInstructions']);
                          @endphp
                          {!!html_entity_decode($data['checkInInstructions'])!!}
                        </p>
                      </td>
                    </tr>
                  @endisset

                  @isset($data['specialCheckInInstructions'])
                  <tr>
                    <td>
                      <h3 style="margin: 0;">Special Check In Instructions</h3>
                      <p style="margin-left: 15px;">{!!html_entity_decode($data['specialCheckInInstructions'])!!}</p>
                    </td>
                  </tr>
                  @endisset
                </tbody>
              </table>
            </td>
          </tr>
        <?php } ?>

        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th>
                      <h2 style="margin: 8px 8px 8px 2px;">Cancellation &amp; Amendment Policy</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul>
                        @isset($data['cancellationPolicy'])
                        <li>{!!html_entity_decode($data['cancellationPolicy'])!!}</li>
                        @endisset
                        @if($data['valueAdds'])
                          @if(count($data['valueAdds']) >= 2)
                            @foreach ($data['valueAdds'] as $key => $value)                            
                                <li>{{$value['description']}}</li>
                            @endforeach
                          @else
                           <li>{{$data['valueAdds']['description']}}</li>

                          @endif
                        @endif
                      </ul>
                    </td>
                  </tr>
                  
                </tbody>
              </table>
            </td>
        </tr>

        <tr>
          <td>
            <p style="margin-top: 40px; font-size: 16px;">We wish you a hassle-free stay.<br>
              Waiting to host you</p>
              
            <p style="margin-top: 20px; font-size: 16px;">See you soon,<br>
              <strong>Team eRoam</strong></p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>