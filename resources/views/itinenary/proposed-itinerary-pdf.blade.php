<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>eRoam Itinerary</title> 
		<link rel="shortcut icon" href="http://dev.eroam.com/images/icons/favicon.ico" >
		<style type="text/css">
		body{
		color: #212121;
		font-size: 14px;
		page-break-inside: avoid;
		}
		p{
		margin: 0px;
		font-size: 13px;
		}
		</style>
	</head>
	<body>
		<?php 
		$logo = getDomainLogo();
		if(!$logo) {
			$logo = url('/images/logo/logo.png');
		}
		?>
	
		<table style="width: 100%;">
			<tr>
			<td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 16px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 4px 17px;">
                   <h4 style="margin-top:7px;margin-bottom:7px;"> <?php echo date("d M Y");?></h2>
                </td>
              </tr>
            </table>
			</td>
			</tr>
		</table>
		
		<?php $chunkArray = array_chunk($search['itinerary'],6); ?>
		@foreach ( $chunkArray as $key1 => $itinerary )
		<div style="{{$key1==0?'width: 100%;  padding-top: 30px; margin-top: 10px;':'width: 100%;'}}">
			<ul style="margin:0;padding: 0; width: 100%; float: left;margin:0;">
				<?php $i = 1; ?>
				@foreach ( $itinerary as $key => $leg ) 
				<?php
				if($key==0){
					$wid = '94px';
				}else if(count($itinerary)==$key+1){
					$wid = '94px';
				}else{
					$wid = '114px';
				}
				?>
				
				<li style="list-style: none;  display: inline-block; width:{{$wid}}; height: 80px; text-align:center">
					@if(count($chunkArray)==$key1+1 && count($itinerary)==$key+1)
						
					@else	
					<div style="width: 100%; height: 2px; border-bottom: dotted 1px #249DD0; display: block; left: {{ ($i == 1)?'43px':'60px'  }}; position: relative; top: 16px; display: block;"></div>
					@endif
					<span class="stepCircle" style="width: 30px; height: 18px; background-color: #ffffff; border: solid 1px #249DD0; border-radius: 50%; margin: 0 auto 10px auto; position: relative; z-index: 1; display: inline-block; padding: 6px 0; color: #249DD0; font-weight: bold;">{{$leg['city']['default_nights']}}</span>
					<p><strong>{{ $leg['city']['name'] }}</strong><br/>{{ $leg['city']['country']['name'] }}</p>
				</li>
				<?php $i++; ?> 
				@endforeach
				<div style="clear: both; display: block; height: 0;"></div>
			</ul> 

			<div style="clear: both;"></div>
		</div>
		@endforeach 
		@foreach ( $search['itinerary'] as $key => $leg )
		<?php
			$last = count($search['itinerary']) - 1;
			$itineraryIndex = $key == $last ? $key : $key + 1;
			$departTimezone = get_timezone_abbreviation($leg['city']['timezone']['name']);
			$arriveTimezone = get_timezone_abbreviation($search['itinerary'][$itineraryIndex]['city']['timezone']['name']);
			?>
		<table style="width: 100%; border: solid 1px #DEDEDE; padding: 0 8px; margin-top: 15px; margin-bottom:5px;" cellpadding="0">
			<tbody>
			<tr>
				<td style="width: 75%;">
					<h4 style="font-size: 16px; line-height: 22px; margin: 5px 0 0 0;"><strong>{{ $leg['city']['name'] }}</strong>, {{ $leg['city']['country']['name'] }}</h4>
					<p>{{ date('d M Y', strtotime($leg['city']['date_from']))}}</p>
				</td>
				<td style="width: 25%;">
					<table>
						<tr>
							<td style="text-align: right; border-right: solid 1px #DEDEDE; padding-right: 15px;">
								<table>
									<tr>
										<td>
											<img src="{{ public_path('/images/ic_local_hotel.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
										<td>
											<img src="{{ public_path('/images/ic_local_activity.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
										<td>
											<img src="{{ public_path('/images/ic_directions_bus.png') }}" alt="" style="margin-right: 8px;"/>
										</td>
									</tr>
								</table>
							</td>
							<?php 
								if($leg['city']['default_nights'] <= 9){
									$total_nights_text= '0'.$leg['city']['default_nights'];
								}else{
									$total_nights_text = $leg['city']['default_nights'];
								}?>	
							<td style="padding-left: 15px; text-align: center;">
								<h3 style="font-size: 30px; margin:0px;"><strong>{{$total_nights_text}}</strong></h3>
								<span style="font-size: 16px;"><?php echo ($leg['city']['default_nights']>1) ? 'Days':'Day' ?></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="width: 100%; border-top: solid 1px #DEDEDE;" cellpadding="6">
						<tr>
							<td style="width: 150px; vertical-align: top;">
								@if(!empty($leg['city']['image']))
								<img style="width:100%;" src="http://cms.eroam.com/{{$leg['city']['image'][0]['small'] }}" alt="" class="img-responsive">
								@else
								<img style="width:100%;" src="{{ public_path('/images/no-image1.jpg') }}" alt="" class="img-responsive">
								@endif
							</td>
							<td style="vertical-align: top;">
								{!! $leg['city']['description'] !!}
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		@if(isset($leg['hotel']) && !empty($leg['hotel']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%; margin-bottom:5px;">
			<tr style="background-color: #D8D8D8;">
				<td colspan="2" style="vertical-align: middle; padding: 5px 10px">
					<span>
						<img src="{{ url('images/ic_local_hotel.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>
					<strong style="font-size: 14px;">ACCOMMODATION</strong>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="border-collapse: collapse; text-align: left; width: 100%;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Location</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Check In</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Check Out</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ date('d M Y', strtotime($leg['city']['date_from']))}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ $leg['hotel']['name'] }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ ($leg['hotel']['provider'] == 'expedia') ? $leg['hotel']['address1'] : $leg['hotel']['address_1']}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;"> {{date('d M Y', strtotime($leg['hotel']['checkin']))}} </td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{date('d M Y', strtotime($leg['hotel']['checkout']))}}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@if(isset($leg['activities']) && !empty($leg['activities']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%;margin-bottom:5px;">
			<tr style="background-color: #D8D8D8;">
				<td style="vertical-align: middle; padding: 5px 10px" colspan="2">
					<span class="m-r-10">
						<img src="{{ public_path('/images/ic_local_activity.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>
				
				<strong style="font-size: 14px;">TOURS / ACTIVITIES</strong></td>
			</tr>
			<tr>
			
				<td colspan="2">
					<table style="border-collapse: collapse; width: 100%; text-align: left;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Location</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Duration</th>
							</tr>
							
						</thead>
						<tbody>
						
							@foreach($leg['activities'] as $activity)
							<tr>
							
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{date('d M Y', strtotime($activity['date_selected']))}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{$activity['name']}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ $leg['city']['name']}}, {{$leg['city']['country']['name']}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">
								<?php 
								if($activity['duration1']){
									$dur_time = explode(' ',$activity['duration1']);
									if(isset($dur_time[2]) && isset($dur_time[3])){
										$duration_time = $dur_time[0].' '.ucfirst($dur_time[1]).' '.$dur_time[2].' '.ucfirst($dur_time[3]);
									}else{
										$duration_time = $dur_time[0].' '.ucfirst($dur_time[1]);
									}
								}else{
									$duration_time = 'N/A';
								}
								?>
								{{ $duration_time }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@if(isset($leg['transport']) && !empty($leg['transport']))
		<table style="border: solid 1px #DEDEDE; border-collapse: collapse; width: 100%;margin-bottom:5px;">
			<tr style="background-color: #D8D8D8;">
				<td style="vertical-align: middle; padding: 5px 10px" colspan="2">
					<span class="m-r-10">
						<img src="{{ public_path('/images/ic_directions_bus.png') }}" alt="" style="margin-right: 8px; margin-top: 2px;"/>
					</span>				
				<strong style="font-size: 14px;">TRANSPORT</strong></td>
			</tr>
			<tr>
				<td colspan="2">
					<table style="border-collapse: collapse; width: 100%; text-align: left;">
						<thead style="background-color: #F3F3F3;">
							<tr>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Date</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Name</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Departure Time</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Arrival / Time</th>
								<th style="background-color: #F3F3F3; padding: 10px 15px; color: #000;">Duration</th>
							</tr>
						</thead>
						<tbody>
							<?php
							
								$travelDate = explode('<br/>', $leg['transport']['departure_text']);
								$travelDate = explode('@', $travelDate[1]);
								$departure_time = explode('<br/>', $leg['transport']['departure_text']);
								$arrival_time = explode('<br/>', $leg['transport']['arrival_text']);
								$changeArrivalTime = explode('@', $arrival_time[1]);
								//echo pr($changeArrivalTime);
							?>
							<?php 
								if($leg['transport']['duration']){
									$transport_duration_new = str_replace('+', '', $leg['transport']['duration']);
									$transport_duration = explode(' ',$transport_duration_new);
									if(isset($transport_duration[4])){
										$transport_duration_text  = $transport_duration[0].' '.ucfirst($transport_duration[1]).' '.$transport_duration[3].' '.ucfirst($transport_duration[4]);
									}elseif(isset($transport_duration[2]) && isset($transport_duration[3])){
										$transport_duration_text  = $transport_duration[0].' '.ucfirst($transport_duration[1]).' '.$transport_duration[2].' '.ucfirst($transport_duration[3]);
									}else{
										$transport_duration_text = $transport_duration[0].' '.ucfirst($transport_duration[1]);
									}
								}else{
									$transport_duration_text = 'N/A';
								}
								?>
							<tr>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ date('d M Y', strtotime($travelDate[0])) }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ $leg['transport']['transport_name_text'] }}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{$departure_time[1]}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;">{{ date('d M Y', strtotime($changeArrivalTime[0])).' @ '}}{{$changeArrivalTime[1]}}</td>
								<td style="border-top: none; padding: 10px 15px; border-bottom: solid 1px #DEDEDE; background-color: #fff;"> 
								
								{{$transport_duration_text}}</td>
				
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		@endif
		@endforeach
	</body>
</html>