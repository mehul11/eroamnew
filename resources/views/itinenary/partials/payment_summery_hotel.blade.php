@if(isset($leg['hotel']) && !empty($leg['hotel']) && $leg['hotel']['provider'] == 'expedia')
  @php 
    global $finalTotalCost;
    $leg['hotel'] = json_decode(json_encode($leg['hotel']), true);
    $singleRate = '@nightlyRateTotal';
    $singleRate = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo'][$singleRate];
    $singleRate = ($singleRate * $eroamPercentage) / 100 + $singleRate;
    $subTotal = $singleRate;
    $taxes = 0;

    $totalNights = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['NightlyRatesPerRoom']['@size'];

    if (isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'])):
      $taxes = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['taxRate'];
      $taxesPerDay = $taxes / $totalNights;
    endif;

    $ratePerDay = $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['ChargeableRateInfo']['@averageBaseRate'];
    $ratePerDay = number_format(($ratePerDay * $eroamPercentage) / 100 + $ratePerDay + $taxesPerDay, 2);

    $selectedRate = number_format($subTotal + $taxes, 2);

    $finalTotalCost = ($subTotal + $taxes) + $finalTotalCost;
  @endphp

   <tr>
       <td>
           <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
           <div class="tour_info cityboxDetails">
               <strong>{{ $leg['hotel']['name'] }}</strong>
               <p> {{date('d M Y', strtotime($leg['hotel']['checkin']))}} - {{date('d M Y', strtotime($leg['hotel']['checkout']))}}</p>
                @isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'])
                {{ $leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['rateDescription'] }}
                @endisset

                @isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes'])
                  @if($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['@size'] > 1)
                    @foreach($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType'] as $bedType)
                      @if(isset($leg['hotel']['bedTypeId']) &&  $leg['hotel']['bedTypeId'] > 0)
                        @if($leg['hotel']['bedTypeId'] == $bedType['@id'])
                          {{','}}<b>{{ucwords($bedType['description'])}}</b>
                          <?php break; ?>
                        @endif
                      @else
                        {{','}}<b>{{ucwords($bedType['description'])}}</b>
                      @endif
                    @endforeach
                  @else
                    {{','}}<b>{{ucwords($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['BedTypes']['BedType']['description'])}}</b>
                  @endif
                @endisset
                
              <p>{{ ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['nonRefundable']) ? "Non-Refundable" : "" }}</p>
           </div>
       </td>
       <td class="text-center">${{$currency}} {{$selectedRate}}</td>
       <td class="text-center">
           <div class="price">
              {{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}
               <!-- <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl disable_item_custom qty_textbox_width" type="text" readonly placeholder="" value="{{$adults}} ADULT{{!empty($children) ? ', '.$children.' CHILD':''}}"></div> -->
       </td>
       <td> 
           <div class="text-right">
                <strong> ${{$currency}} {{$selectedRate}}</strong><br>
                <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
                @if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
                <?php /*<a href="javascript:void(0)" data-target="#cancellationPolicy{{$i}}" data-toggle="modal1">Cancellation Policy</a><br/>*/ ?>
                <span class="cancellation_policy1">
                  <a href="javascript://">Cancellation Policy</a>
                  <div class="cancellation_policybox1"><p>{{$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']}}</p></div>
                </span>
                <br>

                @endif 
                <a href="javascript:void(0)" class="disable_item_custom"">Remove From Itinerary</a>
           </div>
       </td>
   </tr>
    @if(isset($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']) && ($leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy'] != ""))
    <div class="modal fade in" id="cancellationPolicy{{$i}}" tabindex="-1" role="dialog" style="display: none;">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
            <h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
          </div>

            <div class="modal-body">
                <div class="roomType-inner m-t-20">
                  <div class="m-t-20">
                    <p>{{$leg['hotel']['RoomRateDetailsList']['RoomRateDetails']['RateInfos']['RateInfo']['cancellationPolicy']}}</p>
                  </div>

                  <div class="m-t-30 text-right">
                    <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                  </div>
                </div>
              </div>
          </div>
      </div> 
    </div>
    @endif
  <?php $i++;?>

@elseif(isset($leg['hotel']) && !empty($leg['hotel']) && $leg['hotel']['provider'] == 'eroam')
  @php 
    $leg['hotel'] = json_decode(json_encode($leg['hotel']), true);

      $nights  = $leg['city']['default_nights'];
      $roomPrice = $leg['hotel']['default_hotel_room']['price'];
      $roomPrice = $roomPrice * $nights;
      $roomPrice = number_format($roomPrice,2);

    $finalTotalCost += $roomPrice;
  @endphp

   <tr>
       <td>
           <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
           <div class="tour_info cityboxDetails">
               <strong>{{ $leg['hotel']['name'] }}</strong>
               <p> {{date('d M Y', strtotime($leg['hotel']['checkin']))}} - {{date('d M Y', strtotime($leg['hotel']['checkout']))}}</p>
                
              <p>{{ ($leg['hotel']['refundable'] == 1) ? "Refundable" : "Non-Refundable" }}</p>
           </div>
       </td>
       <td class="text-center">${{$currency}} {{$roomPrice}}</td>
       <td class="text-center">
           <div class="price">
              {{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}
               <!-- <input class="form-control form-control-lg shadow-none rounded-0 text-center qty-inputControl disable_item_custom qty_textbox_width" type="text" readonly placeholder="" value="{{$adults}} ADULT{{!empty($children) ? ', '.$children.' CHILD':''}}"></div> -->
       </td>
       <td> 
           <div class="text-right">
                <strong> ${{$currency}} {{ $roomPrice }}</strong><br>
                <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
                @if($leg['hotel']['cancellation_policy'] != "")
                <?php /*<a href="javascript:void(0)" data-target="#cancellationPolicy{{$i}}" data-toggle="modal1">Cancellation Policy</a><br/>*/ ?>
                <span class="cancellation_policy1">
                  <a href="javascript://">Cancellation Policy</a>
                  <div class="cancellation_policybox1"><p>{{$leg['hotel']['cancellation_policy'] }}</p></div>
                </span>
                <br>

                @endif 
                <a href="javascript:void(0)" class="disable_item_custom"">Remove From Itinerary</a>
           </div>
       </td>
   </tr>
  @if(isset($leg['hotel']['cancellation_policy']) && ($leg['hotel']['cancellation_policy'] != ""))
    <div class="modal fade in" id="cancellationPolicy{{$i}}" tabindex="-1" role="dialog" style="display: none;">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header" style="border-bottom: none !important;padding: 15px 15px 0 15px;">
            <h4 class="modal-title" id="gridSystemModalLabel">Cancellation Policy</h4>
          </div>

            <div class="modal-body">
                <div class="roomType-inner m-t-20">
                  <div class="m-t-20">
                    <p>{{$leg['hotel']['cancellation_policy'] }}</p>
                  </div>

                  <div class="m-t-30 text-right">
                    <a href="#" data-dismiss="modal" class="modalLink-blue">CLOSE</a>
                  </div>
                </div>
              </div>
          </div>
      </div> 
    </div>
  @endif

@else
	<tr>
     <td colspan="4">
         <div class="tour_icon cityboxIcon"><i class="ic-local_hotel"></i> </div>
         <div class="tour_info cityboxDetails">
             <strong>Own Arrangement</strong>
			</div>
		</td>
	</tr>
@endif