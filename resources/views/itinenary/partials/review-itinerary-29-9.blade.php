@extends('itinenary.booking-layout')

@section('booking-content')
@php
    $total = $totalCost * $travellers;
    $adults = !empty (session()->get('search_input')['num_of_adults']) ?array_sum(session()->get('search_input')['num_of_adults']):0;
    $children = !empty(session()->get('search_input')['num_of_children']) ? array_sum(session()->get('search_input')['num_of_children']):0;
    if(!empty($adults) && !empty($children)){
        $total = (($adults  + $children) *$totalCost);
    }if(!empty($adults) && empty($children)){
         $total = (($adults) *$totalCost);
    }if(empty($adults) && !empty($children)){
         $total = (($children) *$totalCost);
    }
    
    //$gst = $total*0.025;
    //$finalTotal = number_format($total + $gst, 2, '.', ',');
    $finalTotal = number_format($finalTotalCost + $gst,2,'.',',');
    $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
    $travellers   = session()->get( 'search' )['travellers'];
    $total_childs = session()->get( 'search' )['child_total'];
    $rooms = session()->get( 'search' )['rooms'];
    $search_input = session()->get('search_input');

@endphp
@push('style')
<style type="text/css">
.disable_links{
  cursor: no-drop;
}</style>
@endpush
<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
   
    <div class="container-fluid">
        @include('itinenary.partials.payment-filter-option')

        <div class="itinerary_page pt-2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class=" m-1">
                            <div class="tripDetails mb-3">
                                <div class="card panel border-0 rounded-0 p-3">
                                    <div class="row">
                                        <div class="col-sm-7 col-xl-8">

                                            @if(!empty($data['search_input']['option']) && ($data['search_input']['option'] == 'auto'))
                                                @if(!empty($data['itinerary']) && (count($data['itinerary'])>1) )
                                                <h5 class="font-weight-bold">Multi-City Tailormade Auto</h5>
                                                @else
                                                <h5 class="font-weight-bold">Single-City Tailormade Auto</h5>
                                                @endif
                                            @else
                                                <h5 class="font-weight-bold">Manual Itinerary</h5>
                                            @endif
                                            <div class="media">
                                                <i class="ic-calendar mr-2"></i>
                                                <div class="media-body pb-3 mb-0">
                                                    {{$startDate}} - {{$endDate}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="row align-items-end">
                                                <div class="col-xl-9 col-sm-8 text-left text-sm-right">
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="list-inline-item">
                                                            <i class=" ic-local_hotel"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-local_activity"></i>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <i class="ic-directions_bus"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xl-3 col-sm-4 text-left text-sm-center mt-2 mt-md-0 border-left">
                                                    <h2 class="font-weight-bold mb-0">{{$totalDays}}</h2>
                                                     <?php echo ($totalDays>1) ? 'Days' :'Day'; ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" m-1">

                <div class="row">
                    <div class="col-12">
                        <form  action="{{ url('payment') }}" method="post" name="signup_form" id="signup_form_id" >
                        {{ csrf_field() }}  
                        <div class="accordion" id="accordionExample">
                            <div class="card continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingOne">
                                    <div class="" data-toggle="collapse" data-target="#lead_guest" aria-expanded="true" aria-controls="collapseOne"> 
                                        Lead Guest Information
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>

                                <div id="lead_guest" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>Your contact details are collected in case we need to contact you about your booking. Additional personal information for each guest maybe required post the booking before vouchers can be issued. </p>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="passenger_first_name[0]"  class="form-control passenger_first_name" value="{{ !empty($data['user'])?$data['user']->customer->first_name:'' }}" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family name (As Shown on Passport)</label>
                                                    <input type="text" name="passenger_last_name[0]" class="passenger_last_name form-control" placeholder="Family Name" value="{{ !empty($data['user'])?$data['user']->customer->last_name:'' }}">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="text" name="passenger_email[0]" class="passenger_email form-control"  placeholder="Email Address" value="{{ !empty($data['user'])?$data['user']->customer->email:'' }}">
                                                </div>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="passenger_dob[0]" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control passenger_contact_no" name="passenger_contact_no[0]"  placeholder="Contact Number" value="{{ !empty($data['user'])?$data['user']->customer->contact_no:'' }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if((!empty($search_input['travellers']) && $search_input['travellers']>1) || (!empty($total_childs)))
                            <div class="card continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingTwo">
                                    <div data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Additional Guest
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        
                                        @if(!empty($search_input['travellers']) && $search_input['travellers'] >= 1)

                                            @for($i=1; $i <= $search_input['travellers'] - 1; $i++ )
                                            <h5 class="mb-3">Additional Guest {{ ucfirst(numberTowords($i)) }}</h5>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Adults (18+)</label>
                                                    <div class="custom-select" readonly>
                                                      <select class="disabled" disabled>
                                                         <option>Adult (18+)</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Guest Title</label>
                                                    <div class="custom-select">
                                                        <select class="passenger_title" name="passenger_title[{{$i}}]">
                                                            <option value="">Please Select</option>
                                                            <option value="Mr">Mr</option>
                                                            <option value="Ms">Ms</option>
                                                            <option value="Mrs">Mrs</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="passenger_first_name[{{$i}}]" class="passenger_first_name form-control" placeholder="First Name">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family name (As Shown on Passport)</label>
                                                    <input type="text" name="passenger_last_name[{{$i}}]" class="passenger_last_name form-control" placeholder="Family Name">
                                                </div>
                                            </div>
                                            
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="passenger_dob[{{$i}}]" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="email" name="passenger_email[{{$i}}]" class="form-control passenger_email" placeholder="" value="">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" class="form-control passenger_contact_no"  name="passenger_contact_no[{{$i}}]" placeholder="Contact Number">
                                                </div>
                                            </div>
                                        </div>
                                        @endfor
                                    @endif
                                    @php $j = $travellers; @endphp
                                    @if(!empty($search_input['total_children']) && $search_input['total_children'] >= 1)
                                        @php 
                                        $arr = [];
                                        array_walk_recursive($search_input['child'], function($val, $key) use(&$arr) {
                                                $arr[] = $val;
                                        });

                                        @endphp
                                        @for($i=0; $i < $search_input['total_children']; $i++ )
                                        <h5 class="mb-3">Additional Guest {{ ucfirst(numberTowords($j++)) }}</h5>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Child (0 - 17)</label>
                                                    <div class="custom-select" readonly>
                                                      <select class="disabled" disabled>
                                                        <option>Child (0 - 17)</option>
                                                      </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 col-md-3 form-group">
                                                <div class="fildes_outer">
                                                    <label>Child Age</label>
                                                    <div class="custom-select" readonly name="child_age[$i]">
                                                        <select class="disabled" disabled>
                                                            <option value="1" @if($arr[$i] == '1') ? {{ 'selected' }} : {{ ''}} @endif>1 Year</option>
                                                            <option value="2" @if($arr[$i] == '2') ? {{ 'selected' }} : {{ ''}} @endif>2 Years</option>
                                                            <option value="3" @if($arr[$i] == '3') ? {{ 'selected' }} : {{ ''}} @endif>3 Years</option>
                                                            <option value="4" @if($arr[$i] == '4') ? {{ 'selected' }} : {{ ''}} @endif>4 Years</option>
                                                            <option value="5" @if($arr[$i] == '5') ? {{ 'selected' }} : {{ ''}} @endif>5 Years</option>
                                                            <option value="6" @if($arr[$i] == '6') ? {{ 'selected' }} : {{ ''}} @endif>6 Years</option>
                                                            <option value="7" @if($arr[$i] == '7') ? {{ 'selected' }} : {{ ''}} @endif>7 Years</option>
                                                            <option value="8" @if($arr[$i] == '8') ? {{ 'selected' }} : {{ ''}} @endif>8 Years</option>
                                                            <option value="9" @if($arr[$i] == '9') ? {{ 'selected' }} : {{ ''}} @endif>9 Years</option>
                                                            <option value="10" @if($arr[$i] == '10') ? {{ 'selected' }} : {{ ''}} @endif>10 Years</option>
                                                            <option value="11" @if($arr[$i] == '11') ? {{ 'selected' }} : {{ ''}} @endif>11 Years</option>
                                                            <option value="12" @if($arr[$i] == '12') ? {{ 'selected' }} : {{ ''}} @endif>12 Years</option>
                                                            <option value="13" @if($arr[$i] == '13') ? {{ 'selected' }} : {{ ''}} @endif>13 Years</option>
                                                            <option value="14" @if($arr[$i] == '14') ? {{ 'selected' }} : {{ ''}} @endif>14 Years</option>
                                                            <option value="15" @if($arr[$i] == '15') ? {{ 'selected' }} : {{ ''}} @endif>15 Years</option>
                                                            <option value="16" @if($arr[$i] == '16') ? {{ 'selected' }} : {{ ''}} @endif>16 Years</option>
                                                            <option value="17" @if($arr[$i] == '17') ? {{ 'selected' }} : {{ ''}} @endif>17 Years</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text"  name="child_first_name[{{$i}}]" class="form-control" placeholder="First Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family name (As Shown on Passport)</label>
                                                    <input type="text" name="child_family_name[{{$i}}]" class="form-control" placeholder="Family name">

                                                </div>
                                            </div>
                                        </div>
                                            @endfor
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="card  continue_guest border-0 pl-2 pr-2 pt-2  mb-3">
                                <div class="p-2 heading border-bottom" id="headingThree">
                                    <div class="" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Payment
                                        <a href="#" class="float-right true rounded-circle"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle"><span>+</span>  </a>
                                    </div>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body pb-5">
                                        <h5 class="pb-3 ">Billing Contact Details</h5>

                                        <div class="row">

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>First Name</label>
                                                    <input type="text" name="billing_first_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->first_name:'' }}" placeholder="First Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Family name (As Shown on Passport)</label>
                                                    <input type="text" name="billing_last_name" class="passenger_first_name form-control" value="{{ !empty($data['user'])?$data['user']->customer->last_name:'' }}" placeholder="Family Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Date of Birth</label>
                                                    <input name="billing_passenger_dob" type="text" placeholder="DD MM YYYY" class="form-control passenger_dob datepicker1">
                                                    <span class="arrow_down"><i class="ic-calendar open-datepicker"></i> </span>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Email Address</label>
                                                    <input type="email" name="billing_email" class="passenger_email form-control" value="{{ !empty($data['user'])?$data['user']->customer->email:'' }}" placeholder="Email Address">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Contact Number</label>
                                                    <input type="text" name="billing_contact" class="passenger_contact_no form-control" value="{{ !empty($data['user'])?$data['user']->customer->contact_no:'' }}" placeholder="Contact Number">

                                                </div>
                                            </div>
                                        </div>


                                        <h5>Billing Address</h5>
                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Company Name (Optional)</label>
                                                    <input type="text" name="passenger_company_name" class="form-control" placeholder="Company Name">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Street Address</label>
                                                    <input type="text" name="passenger_address_one" class="form-control" placeholder="Street Address">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Additional Address Information (Optional)</label>
                                                    <input type="text" name="passenger_address_two" class="form-control" placeholder="Additional Address Infomation">

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Suburb / Town </label>
                                                    <input type="text" name="passenger_suburb" class="form-control passenger_suburb" placeholder="Suburb / Town">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>State / Territory / Region </label>
                                                    <input type="text"  name="passenger_state" class="form-control passenger_state" placeholder="State / Territory / Region">
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12  col-md-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Postcode / Area Code</label>
                                                    <input type="text" name="passenger_zip" class="form-control passenger_zip" placeholder="Postcode / Area Code">
                                                </div>
                                            </div>


                                            <div class="col-12 col-sm-12  form-group">
                                                <div class="fildes_outer">
                                                    <label>Country</label>
                                                    <div class="custom-select">
                                                        <select name="passenger_country" class="passenger_country">
                                                            <option value="">Please Select</option>
                                                            @php
                                                            $j=0;
                                                            $allCountry = array();
                                                            foreach($countries as $country){
                                                                foreach ($country['countries_showon_eroam'] as $country_data){
                                                                    $allCountry[$j]['name'] = $country_data['name'];
                                                                    $allCountry[$j]['id'] = $country_data['id'];
                                                                    $allCountry[$j]['region'] = $country['id'];
                                                                    $allCountry[$j]['regionName'] = $country['name'];
                                                                    $j++;
                                                                }
                                                            }
                                                            usort($allCountry, 'sort_by_name');
                                                            @endphp
                                                            @if( count($allCountry) > 0 )
                                                                @foreach($allCountry as $Country)
                                                                    <option value="{{ $Country['id'] }}">{{ $Country['name'] }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="totalAmount" id="totalAmount" value="{{$finalTotal}}">
                                        <input type="hidden" name="currency" id="currency" value="{{$currency}}">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9">
                                                <h5 class="pb-2">Payment Method</h5>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check custom-check-error">I agree to the terms and conditions. &nbsp; <input type="checkbox" checked name="terms" class="terms" id="terms" value="1"><span class="check_indicator">&nbsp;</span></span> <a href="javascript:void(0)" class="blue disable_links">View Terms and Conditions here.</a>
                                                </div>

                                                <div class="form-group black-checkbox mt-3">
                                                    <span class="custom_check">This package pricing is inclusive of flights &nbsp; <input type="checkbox" id="checkbox-04" value="3" checked><span class="check_indicator">&nbsp;</span></span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                                                <img src="{{  url( 'images/visa.jpg') }}" />
                                                <img src="{{  url( 'images/mastercard.jpg') }}" />

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12 col-sm-12 form-group">
                                                <div class="fildes_outer">
                                                    <label>Credit Card Number</label>
                                                    <input type="text" name="card_number" id="card_number" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX">
                                                    <input type="hidden" id="card_valid">
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Month*</label>
                                                    <div class="custom-select" >
                                                        <select name="month" id="month" data-stripe="exp_month" class="month">
                                                            <option value="">MM</option>
                                                            <?php $expiry_month = date('m');?>
                                                            <?php for($i = 1; $i <= 12; $i++) {
                                                            $s = sprintf('%02d', $i);?>
                                                            <option value="<?php echo $s;?>" <?php //if ( $expiry_month == $i ) { ?>  <?php //} ?>><?php echo $s;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 form-group">
                                                <div class="fildes_outer">
                                                    <label>Year*</label>
                                                    <div class="custom-select">
                                                        <select name="year" id="year" data-stripe="exp_year" class="year">
                                                            <option value="">YYYY</option>
                                                            <?php
                                                            $lastyear = date('Y')+21;
                                                            $curryear = date('Y');
                                                            for($k=$curryear;$k<$lastyear;$k++){ ?>
                                                            <option value="<?php echo substr($k, -2);?>"><?php echo $k;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <div class="fildes_outer">
                                                    <label>Security code / CSV</label>
                                                    <input maxlength="3" name="cvv" id="cvv" type="text" placeholder="XXX" class="form-control">
                                                    <span class="arrow_down"><i class="fa fa-question-circle-o"></i> </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 form-group">
                                                <button type="submit" name=""  class="btn  btns_input_dark transform d-block w-100 btn_p">Pay ${{$currency}} {{$finalTotal}} NOW  </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="card disabled continue_guest border-0 pl-2 pr-2 pt-2  mb-3" data-toggle="tooltip" title="@lang('home.create_itenerary_checkbox3_title')">
                                <div class="p-2 heading border-bottom disabled" id="account">
                                    <div class="disabled" data-toggle="collapse" data-target="#accounts" aria-expanded="false" aria-controls="collapseThree">
                                        Account
                                        <a href="#" class="float-right true rounded-circle" style="cursor: not-allowed;"><span> _ </span>  </a>
                                        <a href="#" class="float-right false rounded-circle" style="cursor: not-allowed;"><span>+</span>  </a>
                                    </div>
                                </div>
                            

                            </div>
                        </div>
                         <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td class="border-0">

                                                    <div>  
                                                        <p style="cursor: pointer;"><a  href="javascript:void(0)" class="blue disable_item_custom">Save Itinerary</a>
                                                         </p>

                                                        <p> <a href="javascript:void(0)" class="blue disable_links">View Saved Itineraries</a></p>

                                                        <p> <a href="javascript:void(0)" class="blue disable_links">Enter Promo Code</a> </p>
                                                        <p> &nbsp; </p>

                                                        <p class="pt-2">Contact Us +<a href="javascript:void(0)" class="blue disable_links">61 (0)3 9999 6774 </a></p>

                                                    </div>
                                                </td>
                                                <td colspan="2" class="text-right border-0">
                                                    <!-- <p>Total Per Person</p> -->
                                                    <p>Sub Total Amount</p>
                                                    <p>Credit Card Fee 2.5%</p>
                                                    <p>Local Payment</p>
                                                    <p class="pt-2"><strong>Total Amount</strong></p>

                                                </td>

                                                <td class="text-right border-0">
                                                    <!-- <p>${{$currency}} {{number_format($totalCost, 2, '.', ',')}}</p> -->
                                                    <p>${{$currency}} {{number_format($finalTotalCost, 2, '.', ',')}}</p>
                                                    <p>${{$currency}} {{number_format($gst, 2, '.', ',')}}</p>
                                                    <p>${{$currency}} {{number_format($finalTotalCost,2, '.', ',')}}</p>

                                                    <p class="pt-2"><strong>${{$currency}} {{$finalTotal}}</strong></p>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12"> 
                                    I understand and agree that I am entering into an electronic transaction, and by clicking Continue, I also accept and agree to all terms of eRoam’s <a href="javascript:void(0)" class="blue disable_links">Sales and Refund Policy</a>. I agree to receive electronically all documents, including warranty, disclosure and/or insurance documents, as applicable, to my nominated email address. Your contact details are collected in case we need to contact you about your booking. If you do not complete your booking, then by clicking the ‘Continue’ button below you agree that we may contact you by email or by telephone (including by SMS) to follow up regarding your incomplete booking. For more information, please see our privacy policy. <a href="{{URL::to('privacy-policy')}}" class="blue" target="_blank">Click Here</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
   <script src="{{url('js/payform.min.js')}}"></script>
   <script src="{{url('js/itinerary/common.js')}}"></script>
   <script src="{{url('js/booking-summary.js')}}"></script>
   <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
@endpush