   
@extends('layouts.common')
@section('content')
@include('partials.banner')
@include('partials.search')
<div class="account-block">
    @include('users.partials.sidebar')
    <div class="account-right p-4 pl-5">
        @include('users.partials.complete_profile')
        @if($link != '')
        <div class="mt-4 row">
            <div class="col-xl-4 offset-xl-8 col-sm-6 offset-sm-6">
                <div class="row">
                    <div class="col-sm-5 col-5">
                        <a href="{{ $link }}" class="btn btns_input_dark def_sign_btn btn-block">UPDATE</a>
                    </div>
                    <div class="col-sm-7 col-7">Update your personal travel preferences.</div>
                </div>
            </div>
        </div>
        @endif
        <hr class="mt-5">
        @if(session()->has('profile_step1_success'))
        <div class="alert alert-success text-center" role="alert">
            {{ session()->get('profile_step1_success') }}
        </div>
        @endif
        @if(session()->has('profile_step1_error'))
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach(session()->get('profile_step1_error') as $msg)
                <li>{{$msg}}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <h5 class="mt-4">Your Profile</h5>
        <form class="mt-3" method="post" action="{{url('profile_step1')}}" id="profile_step1_form">
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Title *</label>
                    <div class="custom-select">
						{{Form::select('title',[''=>'Select Title','mr'=>'Mr','mrs'=>'Mrs','ms'=>'Ms'] ,array_key_exists('title',old())?old('title'):$user->customer->title,['class'=>'form-control','id'=>'title'])}}
                    </div>
					@if ($errors->has('title')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('title')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label for="first_name">First Name *</label>
                    <input type="text" name="first_name" value="{{ array_key_exists('first_name',old())?old('first_name'):$user->customer->first_name }}" class="form-control" placeholder="" />  
                    @if ($errors->has('first_name')) 
                    <label generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('first_name')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">  
                    <label>Last Name *</label>
                    <input type="text" name="last_name" value="{{ array_key_exists('last_name',old())?old('last_name'):$user->customer->last_name }}" class="form-control" placeholder="" /> 
                    @if ($errors->has('last_name')) 
                    <label generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('last_name')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Email Address</label>
                    <input type="text" name="email" readonly value="{{ array_key_exists('email',old())?old('email'):$user->customer->email }}" class="form-control" placeholder="" />
                
                    @if($user->active == 0)
                    <label for="email" generated="true" class="error">Please confirm your email address</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Contact Number *</label>
                    <input type="text" name="contact_no" class="form-control" placeholder="" value="{{ array_key_exists('contact_no',old())?old('contact_no'):$user->customer->contact_no}}"/>
                    @if ($errors->has('contact_no')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('contact_no')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Enter Existing Password</label>
                    <input type="password" name="old_password" id="old_password" class="form-control" placeholder="" />  
                    @if ($errors->has('old_password')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('old_password')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>New Password</label>
                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="" />
                    @if ($errors->has('new_password')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('new_password')}}</label>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="fildes_outer mb-1">
                    <label>Confirm New Password</label>
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="" />
                    @if ($errors->has('confirm_password')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('confirm_password')}}</label>
                    @endif
                </div>
                <br/>
                Must be at least 8 characters, and ideally contain some or all of: CAPS and lower case, numbers, random characters like #!*.
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Gender</label>
                    <div class="custom-select">
                        {{Form::select('pref_gender',['male'=>'Male','female'=>'Female','other'=>'Other'] ,array_key_exists('pref_gender',old())?old('pref_gender'):$user->customer->pref_gender,['class'=>'form-control','id'=>'pref_gender','placeholder'=>'Select Gender *'])}}
                    </div>
                </div>
                 @if ($errors->has('pref_gender')) 
                <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_gender')}}</label>
                @endif
                 <label for="pref_gender" generated="true" class="text-danger mt-1 error" style="display: inline-block;">
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Age Group *</label>
                    <div class="custom-select">
                        @php
                        $age_group_array = array();
                        foreach ($travel_preferences['age_groups'] as $age_group){
                        $age_group_array[$age_group['id']] = $age_group['name'];
                        }
                        @endphp
                        {{Form::select('pref_age_group_id',$age_group_array,array_key_exists('pref_age_group_id',old())?old('pref_age_group_id'):$user->customer->pref_age_group_id,['class'=>'form-control','id'=>'age-group','placeholder'=>'Select Age Group'])}}
                    </div>
                </div>
                 @if ($errors->has('pref_age_group_id')) 
                <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_age_group_id')}}</label>
                @endif
                <label for="age-group" generated="true" class="text-danger mt-1 error" style="display: inline-block;">
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Nationality *</label>
                    <div class="custom-select">
                        @php
                        $nationality_array = array();
                        foreach ($travel_preferences['nationalities']['featured'] as $nationality){
                        $nationality_array[$nationality['id']] = $nationality['name'];
                        }
                        $nationality_array['disabled'] = '========================================';
                        foreach ($travel_preferences['nationalities']['not_featured'] as $nationality){
                        $nationality_array[$nationality['id']] = $nationality['name'];
                        }
                        @endphp
                        {{Form::select('pref_nationality_id',$nationality_array,array_key_exists('pref_nationality_id',old())?old('pref_nationality_id'):$user->customer->pref_nationality_id,['class'=>'form-control','id'=>'nationality','placeholder'=>'Select Nationality'])}}
                    </div>
                </div>
                 @if ($errors->has('pref_nationality_id')) 
                <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('pref_nationality_id')}}</label>
                @endif
                 <label for="nationality" generated="true" class="text-danger mt-1 error" style="display: inline-block;">
            </div>
            <div class="form-group">
                <div class="fildes_outer">
                    <label>Default Currency *</label>
                    <div class="custom-select">
                        @php
                        $currency_group_array = array();
                        foreach (session()->get('all_currencies') as $currency){
                        $currency_group_array[$currency['code']] = '('.$currency['code'].') '.$currency['name'];
                        }
                        @endphp
                        {{Form::select('currency',$currency_group_array,array_key_exists('currency',old())?old('currency'):$user->customer->currency,['class'=>'form-control','id'=>'select-currency','placeholder'=>'Select Default Currency'])}}
                    </div>
                    
                </div>
                @if ($errors->has('currency')) 
                    <label for="first_name" generated="true" class="text-danger mt-1 error" style="display: inline-block;">{{$errors->first('currency')}}</label>
                    @endif
                <label for="select-currency" generated="true" class="text-danger mt-1 error" style="display: inline-block;">
            </div>
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="step" value="1">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">
            <input type="hidden" id="registered_user_id" value="{{ $user->customer->email }}">
            <button type="submit" class="btn btns_input_dark def_sign_btn btn-block mt-5 pt-2 pb-2">UPDATE PROFILE</button>
        </form>
    </div>
</div>
@include('home.partials.custom-js')
@include('partials.custom-js')
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ url('js/users/user-validation.js') }}"></script>
@endpush