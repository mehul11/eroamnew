@extends('layouts.static')


@section('custom-css')
<style>
	.wrapper {
		background-image: url( {{ url( 'images/bg1.jpg' ) }} ) !important;
	}
	.top-margin{
		margin-top: 5px;
	}
	.container-padding{
		padding-left: 35px;
		padding-right: 35px;
	}
	.top-bottom-paddings{
		padding-top: 20px;
		padding-bottom: 20px;
	}
	.white-box{
		background: rgba(255, 255, 255, 0.9);
		border: solid thin #9c9b9b !important;
	}

	.wrapper{
		background-attachment: fixed;
	}

	#contact-us p{
		line-height: 1.3em;
		text-align: justify;
	}
	.padding-20{
		padding-left: 20px;
		padding-right: 20px;
	}
	.text-field{
	    border: 1px solid #000;
	    padding: 11px;
	    width: 100%;
	    background: #fff;
	    border-radius: 2px;
	}
	.blue-button{
		transition: all 0.5s;
		background: #2AA9DF;
		border-color: #2AA9DF;
		color: #fff;
		padding: 11px;
		font-weight: bold;
		border-radius: 2px;
		width: 100%;
	}
	.blue-button:hover{
		background: #BEE5F6;
		color: #2AA9DF;
		border-color: #2AA9DF;
	}
	.has-error .text-field {
	    border-color: #a94442;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	}
	
</style>
@stop

@section('content')

	<div class="top-section-image">
		<img src="{{asset('images/bg-image.jpg')}}" alt="" class="img-responsive">
	</div>

	<section class="content-wrapper">
		<h1 class="hide"></h1>
		<article class="about-section">
			<div class="container">
				@if(session()->has('message'))
					<p class="success-box">{{ session()->get('message') }}</p>
				@endif
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-sm-12 col-md-5">
								<h2>Email Us</h2>
								<form name="myForm" id="myForm" action="" method="post">
									{{ csrf_field() }}
								 
									<div class="form-group">
									<label for="name" >Enter Your Name</label>
										<input type="text" class="form-control" id="name" name="name" >
										
									</div>
									<div  class="form-group">
									<label for="email">Enter Your Email</label>
										<input type="email"  class="form-control" id="email" name="email" >
										
									</div>
									<div  class="form-group">
									<label for="msg">Enter Your Message</label>
										<textarea name="message"  class="form-control" rows="20" style="height: 150px;"></textarea>
										
									</div>
									<div>
										<button type="submit" name="myForm" id="myForm" class="btn btn-primary btn-block border-0">Submit</button>
									</div>
								 
								</form>
							</div>
							<div class="col-sm-12 col-md-1">
							
                            </div>
							
							<div class="col-sm-12 col-md-6">
								<h2>Contact Information</h2>
								<div class="mt-4">
									<p><strong>Australia / Call Centre</strong><br/>1 800 803 205 (Hours 09:00 to 17:00 Mon to Fri - AEST)</p>
									<p><strong>USA / Canada Call Center</strong><br/>1 800 935 0857 (Hours 17:00 to Midnight Sun to Thu - PST)</p>
									<p><strong>UK &amp; Europe Call Centre</strong><br/>+44 (0)20 3608 1343 (Hours 06:00 to 12:00 Mon to Fri - GMT)</p>
									<p><strong>REST OF THE WORLD</strong><br/>+613 9999 6774 (Hours 09:00 to 17:00 Mon to Fri - AEST)</p>
								</div>
								<div class="mt-5">
									<h2>Chat With Us</h2>
									<div class="mt-4">
										<a href="" id="support-btn" onclick="triggerChat()"><img src="https://image.providesupport.com/image/10u9zj2w12fal0hhwl1jxaoifj/current" style="border:0px" alt=""></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>

	</section>
	

@stop

@section( 'custom-js' )
<script>
	$("#myForm").validate({
	rules: {
        email: {
			required: true,
			email:true
		},
		name: {
			required: true
		},
        message: {
            required: true
        }
	},
	messages: {

	email: {
		required: "Please enter Email Address."
	}

	},
	errorPlacement: function (label, element) {
		label.insertAfter(element);
	},
	submitHandler: function (form) {
        form.submit();
	}
	});
</script>
@stop