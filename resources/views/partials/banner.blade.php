<section class="banner-bg">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url('{{  url( 'images/banner/NUBIANS-AND-BEACHES.jpg') }}'); background-size: cover; ">
                <div class="container">
                    <h1>@lang('home.banner_title1')</h1>
                    <p>@lang('home.banner_tour_days1')</p>
                    <h2>@lang('home.banner_price1')</h2>
                    <a href="{{ url('tourDetail/11374/live_the_australian_surf_lifestyle_3_month_learn_to_surf_academy1') }}" class="btn viewoffer btn-theme">@lang('home.link_view_offer')</a>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('{{  url( 'images/banner/EVEREST-BASE-CAMP.jpg') }}'); background-size: cover; ">
                <div class="container">
                    <h1>@lang('home.banner_title2')</h1>
                    <p>@lang('home.banner_tour_days2')</p>
                    <h2>@lang('home.banner_price2')</h2>
                    <a href="{{ url('tourDetail/11372/live_the_australian_surf_lifestyle_1_month_learn_to_surf_academy1') }}" class="btn viewoffer btn-theme">@lang('home.link_view_offer')</a>
                </div>
            </div>
            <div class="carousel-item" style="background-image: url('{{  url( 'images/banner/COOKTOWN-EXPLORER.jpg') }}'); background-size: cover; ">
                <div class="container">
                    <h1>@lang('home.banner_title3')</h1>
                    <p>@lang('home.banner_tour_days3')</p>
                    <h2>@lang('home.banner_price3')</h2>
                    <a href="{{ url('tourDetail/11378/2_day_great_barrier_reef_snorkel_daintree_rainforest_and_aboriginal_culture_tour') }}" class="btn viewoffer btn-theme">@lang('home.link_view_offer')</a>
                </div>
            </div>
        </div>
    </div>
</section>