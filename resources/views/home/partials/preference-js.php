<script type="text/javascript">
	$('body').on('click', '#save_travel_preferences', function() {
			var btn = $(this).text('loading..');

			if($('.nationality_id').val()){
			  var nationality_dom = $(".nationality option:selected").text();
			}else{
			  var nationality_dom = $(".nationality option:selected").val();
			}
				
			if($('.age_group').val()){
			  var age_group_dom = $(".age_group option:selected").text();
			}else{
			  var age_group_dom = $(".age_group option:selected").val();
			}
			var age_group_id = $('.age_group').val();
			var nationality_id = $('.nationality').val();
			var gender = $('.gender').val();
		   
			var nationality = ( isNotUndefined( nationality_dom ) ) ? nationality_dom : [] ;
			var age_group = ( isNotUndefined( age_group_dom ) ) ? age_group_dom : [] ;
			var interestLists = [];
			var accommodations = [];
			var accommodationIds = [];
			var interestListIds = [];
			var roomTypeIds = [];
			var roomTypes = [];
			var transportTypeIds = [];
			var transportTypes = [];
              
			$('.hotel_category option:selected').each(function(){
			  if($(this).val()){
				var id = parseInt($(this).val());
				var name = $(this).attr('data-name');
				accommodationIds.push(id);
				accommodations.push(name);
			  }
			});

			$('.room_type_options option:selected').each(function(){
			  var id = parseInt($(this).val());
			  var name = $(this).attr('data-name');
			  roomTypeIds.push(id);
			  roomTypes.push(name);
			});
			
			$('.transport_type_options option:selected').each(function(){
			  if($(this).val()){
				var id = parseInt($(this).val());
				var name = $(this).attr('data-name');
				transportTypeIds.push(id);
				transportTypes.push(name);
			  }
			});
            
			$('.interest-button-active').each(function(){
				interestLists.push($(this).attr('data-name'));
			});
			
			var  data = { 
			  travel_preference: [{
				accommodation:accommodationIds, 
				accommodation_name: accommodations,
				room_name: roomTypes,
				room: roomTypeIds,
				transport_name: transportTypes,
				transport: transportTypeIds,
				age_group: age_group,
				nationality: nationality,
				gender: gender,
				interestLists: interestLists.join(', '),
				interestListIds: activitySequence
				//interestListIds: interestListIds
			  }]
			};
				

			var interestText = interestLists.length > 0 ? interestLists.join(', ') : 'All';
			var accommodation = accommodations.length > 0 ? accommodations.join(', ') : 'All';
			var transport = transportTypes.length > 0 ? transportTypes.join(', ') : 'All';
			var nationalityText =  nationality !=''  ? nationality : 'All'; 
		  
			var ageGroupText =  age_group !='' ? age_group : 'All'; 

			$('#_accommodation').html(' <strong> Accommodation: </strong> '+accommodation);
			$('#_transport').html(' <strong> Transport: </strong> '+transport);
			$('#_nationality').html(' <strong> Nationality: </strong> '+nationalityText);
			$('#_age').html(' <strong> Age: </strong> '+ageGroupText);
			$('#_interests').html(' <strong> Interests: </strong> '+ interestText);
            

			@if (session()->has('user_auth'))
			  var post = {
				hotel_categories:accommodationIds, 
				hotel_room_types: roomTypeIds,
				transport_types: transportTypeIds,
				age_group: age_group_id,
				nationality: nationality_id,
				gender: gender,
				interests: activitySequence,
				_token: $('meta[name="csrf-token"]').attr('content'),
				user_id: "{{ session()->get('user_auth')['user_id'] }}",
			  };
			  eroam.ajax('post', 'save/travel-preferences', post, function(response){
				
			  });
			@endif;

			eroam.ajax('post', 'session/travel-preferences', data, function(response){
				setTimeout(function() {
				btn.text('Accept');
				btn.button('reset');

					eroam.ajax('get', 'session/updatePreferences', '', function(responsedata){
						$("#changePreferences").html(responsedata);
					});

					$('#preferencesModal').modal('hide');
				}, 5000);
			});
		});

	function formatDate(date,type) {
	    var monthNames = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"];
	    var day = date.getDate();
	    var monthIndex = date.getMonth();
	    var year = date.getFullYear();
	    if(type == 'start'){
	        return day + ' ' + monthNames[monthIndex];
	    }else{
	        return day + ' ' + monthNames[monthIndex] + ' ' + year;
	    }
	}
</script>
<script>
	var body = $('body');
	var modal = $('#preferencesModal');

	function showModal() {
		body.css('overflow','hidden');
		modal.show().css('overflow', 'auto');
	}

	function hideModal() {
	   body.css('overflow','auto');
	   $('body').css('overflow','auto')
	   modal.hide();
	}

	$('.profile_popup').on('click', showModal);
	$('.close').on('click', hideModal);
</script>
