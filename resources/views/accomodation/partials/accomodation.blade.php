<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
	@include('accomodation.partials.filters')
	<div class="itinerary_page pt-2">
		<div class="container-fluid">
			<div id="hotels-container1" class="loader_container">
				<div id="hotel-loader">
					<div><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.loading_hotel_text')  </div>
				</div>
			</div>
			<div class="hotel-list-selected p-3">
			</div>
			<div class="hotel-grid-selected row p-3" style="display: none;">
			</div>
			<div class="hotel-list1 p-3" id="listview">  
			</div>
			<div class="hotel-list2 row p-3" id="gridview" style="display: none;">  
			</div>

			<div id="noResult"></div>
		</div>
	</div>
</div>