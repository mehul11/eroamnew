<?php

namespace App\Service\Mystifly;

/**
 * 
 */
class FareRules
{
	

	 public function getFareRules($FareSourceCode = ''){
        $pathHelper = resolve('PathHelper');
        $api_url = $pathHelper->getApiPath();
        $domain = $pathHelper->getDomain();
        
        if(empty($FareSourceCode)){
            $data = array('FareSourceCode'=>trim(request()->input('FareSourceCode')));
        }else{
            $data = array('FareSourceCode'=>$FareSourceCode);
        }
        $request = TRUE;
        while( $request ){
                try{
                    $client = new \GuzzleHttp\Client(['headers' => ['domain' => $domain]]);
                    $response = $client->request('post',$api_url . 'mystifly/fareRules', [
                        'form_params' => $data
                    ]);
                    $request = FALSE;
                }catch(Exception $e){
                Log::error('An error has occured during a guzzle call on MystiflyController@set for provider "'.$provider.'" '.'with key "'.$key.'" and ERROR MESSAGE:'.$e->getMessage() );
            }
        }
                
        $result = json_decode( $response->getBody() , true );
        $category = array();
        $baggage = array();
        $rules = array();
        $data = $result['data'];
        if(!empty($data)){
            if(isset($data['BaggageInfos']['BaggageInfo']) && !empty($data['BaggageInfos']['BaggageInfo']))
            {
                if(!isset($data['BaggageInfos']['BaggageInfo'][0]))
                {
                    $tempData = $data['BaggageInfos']['BaggageInfo'];
                    unset($data['BaggageInfos']['BaggageInfo']);
                    $data['BaggageInfos']['BaggageInfo'][0] = $tempData; 
                }

                $baggage =  $data['BaggageInfos']['BaggageInfo'];

            }
            if(isset($data['FareRules']) && !empty($data['FareRules']))
            {

                if(isset($data['FareRules']['FareRule']) && !empty($data['FareRules']['FareRule']))
                {
                    if(isset($data['FareRules']['FareRule'][0])){
                        foreach($data['FareRules']['FareRule'] as $key => $val)
                        {
                            if(isset($val['RuleDetails']['RuleDetail']) && !empty($val['RuleDetails']['RuleDetail'])){
                                foreach($val['RuleDetails']['RuleDetail'] as $key1 => $val1)
                                {
                                    if(isset($val1['Category']))
                                    {
                                        array_push($category, $val1['Category']);
                                        array_push($rules, $val1['Rules']);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        $val = $data['FareRules']['FareRule'];
                        if(isset($val['RuleDetails']['RuleDetail']) && !empty($val['RuleDetails']['RuleDetail']))
                        {
                            foreach($val['RuleDetails']['RuleDetail'] as $key1 => $val1)
                            {
                                if(isset($val1['Category']))
                                {
                                    array_push($category, $val1['Category']);
                                    array_push($rules, $val1['Rules']);
                                }
                            }
                        }
                    }
                }
            }
        }

        $return['rules'] = $rules;
        $return['category'] = $category;
        $return['baggage'] = $baggage;
        return $return;
        
    }
}