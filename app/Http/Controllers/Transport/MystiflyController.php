<?php

namespace App\Http\Controllers\Transport;

use App\Http\Controllers\Controller;
use Guzzle;
use Input;
use App\Service\Mystifly\FareRules;

class MystiflyController extends Controller
{
	public function fareRules(FareRules $fareRules)
    {

        $return = $fareRules->getFareRules();
        $rules = $return['rules'];
        $category = $return['category'];
        $baggage = $return['baggage'];

        return view('transport.view-fare-rules')->with(compact('rules','category','baggage'))->render();
    }
}
